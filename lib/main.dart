import 'package:coviapp_th/repositories/data_repositories.dart';
import 'package:coviapp_th/ui/main_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  //await _updateData();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Provider<DataRepository>(
      create: (_) => DataRepository(),
      child: MaterialApp(
          debugShowCheckedModeBanner: false,

          //return MaterialApp(
          title: 'Covi App',
          theme: ThemeData.dark().copyWith(
            scaffoldBackgroundColor: Color(0xFF101010),
            cardColor: Color(0xFF222222),
          ),
          home: MainPage()
          //home: MyHomePage(title: 'Flutter Demo Home Page'),
          ),
    );
  }
}
