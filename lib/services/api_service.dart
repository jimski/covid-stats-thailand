import 'dart:convert';
import 'package:coviapp_th/services/data_model.dart';
import 'package:flutter/foundation.dart';
import 'api.dart';
import 'package:http/http.dart' as http;

class APIService {
  APIService(this.api);
  final API api;

  Future<Data> getEndpointData({@required Endpoint endpoint}) async {
    final uri = api.endpointUri(endpoint);
    final response = await http.get(
      uri,
      //headers: {'application/Type'},
    );
    print('mcoke= response.statusCode=' + response.statusCode.toString());
    if (response.statusCode == 200) {
      //final List<dynamic> data = json.decode(response.body);
      final dynamic data = json.decode(response.body);
      print(data);
      if (data.isNotEmpty) {
        final Map<String, dynamic> endpointData = data;
        return Data.createResult(endpointData);
      }
    }

    throw response;
  }
}
