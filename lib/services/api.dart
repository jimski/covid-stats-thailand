import 'package:flutter/foundation.dart';

enum Endpoint {
  today,
  newConfirmed,
  newRecovered,
  newDeaths,
  newHospitalized,
  confirmed,
  recovered,
  deaths,
  hospitalized,
  updateDate,
}

//https://covid19.th-stat.com/api/open/today

class API {
  API({@required this.apiKey});
  final String apiKey;

  static final String host = "covid19.th-stat.com";
  factory API.sandbox() => API(apiKey: '');

  static Map<Endpoint, String> _paths = {
    Endpoint.today: '/api/open/today',
  };
  Uri endpointUri(Endpoint endpoint) => Uri(
        scheme: 'https',
        host: host,
        path: _paths[endpoint],
      );
}
