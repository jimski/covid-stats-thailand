/**
// https://covid19.th-stat.com/api/open/today
{
  "Confirmed": 39038,
  "Recovered": 28480,
  "Hospitalized": 10461,
  "Deaths": 97,
  "NewConfirmed": 1582,
  "NewRecovered": 97,
  "NewHospitalized": 1485,
  "NewDeaths": 0,
  "UpdateDate": "16/04/2021 16:05",
  "Source": "https://covid19.th-stat.com/",
  "DevBy": "https://www.kidkarnmai.com/",
  "SeverBy": "https://smilehost.asia/"
}
 */

class Data {
  String confirmed;
  String recovered;
  String deaths;
  String newConfirmed;
  String newRecovered;
  String newDeaths;
  String updateDate;
  String newHospitalized;
  String hospitalized;

  Data({
    this.confirmed,
    this.recovered,
    this.deaths,
    this.newConfirmed,
    this.newRecovered,
    this.newDeaths,
    this.newHospitalized,
    this.updateDate,
    this.hospitalized,
  });

  factory Data.createResult(Map<String, dynamic> object) {
    return Data(
        // today: "today",
        confirmed: object['Confirmed'].toString(),
        recovered: object['Recovered'].toString(),
        deaths: object['Deaths'].toString(),
        newConfirmed: object['NewConfirmed'].toString(),
        newRecovered: object['NewRecovered'].toString(),
        newDeaths: object['NewDeaths'].toString(),
        newHospitalized: object['NewHospitalized'].toString(),
        updateDate: object['UpdateDate'],
        hospitalized: object['Hospitalized'].toString());
  }
}
