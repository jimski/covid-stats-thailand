import 'dart:io';

import 'package:coviapp_th/repositories/data_repositories.dart';
import 'package:coviapp_th/services/api.dart';
import 'package:coviapp_th/services/api_service.dart';
import 'package:coviapp_th/ui/2nd_swipe.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '3rd_swipe.dart';
import 'alert_dialog.dart';
import 'last_update_date.dart';
import 'mainpage_card.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  Map<Endpoint, dynamic> _dataResult2;
  final apiService = APIService(API.sandbox());
  //DataRepository _dataRepository;

  Future<void> _getUpdatedData() async {
    try {
      final _dataRepository =
          Provider.of<DataRepository>(context, listen: false);
      final data = await _dataRepository.getUpdatedData;
      setState(() {
        _dataResult2 = data;
      });
    } on SocketException catch (_) {
      // TODO
      showAlertDialogBox(
          context: context,
          title: 'There is a connection Error',
          content: 'Could not retrieve data , try again in another time',
          defaultActionText: 'OK');
    } catch (_) {
      {
        // TODO
        showAlertDialogBox(
            context: context,
            title: 'There is an Error',
            content: 'Please contact Admin',
            defaultActionText: 'OK');
      }
    }
    // _dataResult2 = await new DataRepository().getUpdatedData;
    print(_dataResult2);
    print("initState = " + _dataResult2.toString());
  }

  @override
  initState() {
    super.initState();
    _getUpdatedData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Covid App Statistics - Thailand'),
      ),
      body: PageView(
        children: [
          RefreshIndicator(
            onRefresh: _getUpdatedData,
            child: ListView(
              children: <Widget>[
                LastUpdateDate(
                    text: _dataResult2 != null
                        ? 'Last update : ' + _dataResult2[Endpoint.updateDate]
                        : ''),
                for (var endpoint in Endpoint.values)
                  if (endpoint != Endpoint.today &&
                      endpoint != Endpoint.updateDate)
                    MainPageCard(
                      endpoint: endpoint,
                      value:
                          _dataResult2 != null ? _dataResult2[endpoint] : null,
                    ),
              ],
            ),
          ),
          infograpptips(),
          infoText(),
        ],
      ),
    );
  }
}
