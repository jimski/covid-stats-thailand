import 'package:flutter/material.dart';

class infograpptips extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black12,
      child: ListView(
        children: [
          Column(
            children: <Widget>[
              Row(
                children: [
                  Image.asset(
                    'assets/1_d.jpg',
                    height: MediaQuery.of(context).size.width / 1.5,
                    width: MediaQuery.of(context).size.width / 2,
                    fit: BoxFit.fill,
                  ),
                  Image.asset(
                    'assets/2_m.jpg',
                    height: MediaQuery.of(context).size.width / 1.5,
                    width: MediaQuery.of(context).size.width / 2,
                    fit: BoxFit.fill,
                  ),
                ],
              ),
              Row(
                children: [
                  Image.asset(
                    'assets/3_h.jpg',
                    height: MediaQuery.of(context).size.width / 1.5,
                    width: MediaQuery.of(context).size.width / 2,
                    fit: BoxFit.fill,
                  ),
                  Image.asset(
                    'assets/4_t.jpg',
                    height: MediaQuery.of(context).size.width / 1.5,
                    width: MediaQuery.of(context).size.width / 2,
                    fit: BoxFit.fill,
                  ),
                ],
              ),
              Row(
                children: [
                  Image.asset(
                    'assets/5_t.jpg',
                    height: MediaQuery.of(context).size.width / 1.5,
                    width: MediaQuery.of(context).size.width / 2,
                    fit: BoxFit.fill,
                  ),
                  Image.asset(
                    'assets/6_a.jpg',
                    height: MediaQuery.of(context).size.width / 1.5,
                    width: MediaQuery.of(context).size.width / 2,
                    fit: BoxFit.fill,
                  ),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}
