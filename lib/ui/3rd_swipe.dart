import 'package:flutter/material.dart';

class infoText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.alphaBlend(Colors.black12, Colors.black87),
      child: Center(
        child: Column(

            //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                margin: EdgeInsets.fromLTRB(5, 120, 5, 20),
                child: Text(
                  'Stay Safe',
                  style: TextStyle(
                    fontSize: 100,
                    fontWeight: FontWeight.w800,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              Text(
                'ขอให้ปลอดภัย',
                style: TextStyle(
                  fontSize: 50,
                  fontWeight: FontWeight.w300,
                ),
                textAlign: TextAlign.center,
              ),
            ]),
      ),
    );
  }
}
