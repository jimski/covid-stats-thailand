import 'package:coviapp_th/services/api.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class MainPageCardData {
  MainPageCardData(this.title, this.assetName, this.color);
  final String title;
  final String assetName;
  final Color color;
}

class MainPageCard extends StatelessWidget {
  const MainPageCard({Key key, this.endpoint, this.value}) : super(key: key);
  final Endpoint endpoint;
  final String value;

  static Map<Endpoint, MainPageCardData> _cardsData = {
    Endpoint.today: MainPageCardData(
        'Today', 'assets/1_total_confirm.png', Color(0xFFFFF492)),
    Endpoint.confirmed: MainPageCardData('Total Confirmed Cases',
        'assets/1_total_confirm.png', Color(0xFFFFFFF3)),
    Endpoint.recovered: MainPageCardData(
        'Total Recovered', 'assets/8_suspect.png', Color(0xFF70A901)),
    Endpoint.deaths:
        MainPageCardData('Total Deaths', 'assets/6_dead_lucu.png', null),
    Endpoint.newConfirmed: MainPageCardData(
        '[New Confirmed]', 'assets/5_new_confirmed.png', Color(0xFFE40000)),
    Endpoint.newRecovered: MainPageCardData(
        '[New Recovered]', 'assets/2_patient.png', Color(0xFF70A901)),
    Endpoint.newDeaths: MainPageCardData(
        '[New Deaths]', 'assets/3_new_death.png', Color(0xFFE40000)),
    Endpoint.newHospitalized: MainPageCardData(
        '[New Hospitalized]', 'assets/4_fever.png', Color(0xFFE99600)),
    Endpoint.hospitalized: MainPageCardData('Total Hospitalized',
        'assets/7_total_hospitalize.png', Color(0xFFEEDA28)),
  };

  String get formattedValue {
    if (value == null) {
      return '';
    }
    return NumberFormat('#,###,###,###').format(int.parse(value));
  }

  @override
  Widget build(BuildContext context) {
    final cardData = _cardsData[endpoint];
    print("mcoke build");
    print(cardData);
    print(endpoint);
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
      child: Card(
          child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              _cardsData[endpoint].title,
              style: Theme.of(context)
                  .textTheme
                  .headline5
                  .copyWith(color: cardData.color),
            ),
            SizedBox(height: 5),
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                // crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image.asset(cardData.assetName,
                      color: (cardData != null ? cardData.color : null)),
                  Text(
                    (value != null ? (formattedValue) : null),
                    style: Theme.of(context)
                        .textTheme
                        .headline4
                        .copyWith(color: cardData.color),
                  ),
                ]),
          ],
        ),
      )),
    );
  }
}
