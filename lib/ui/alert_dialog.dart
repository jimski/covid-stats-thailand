import 'package:flutter/material.dart';

Future<void> showAlertDialogBox(
    {@required BuildContext context,
    @required title,
    @required content,
    @required defaultActionText}) async {
  return await showDialog(
      context: context,
      builder: (context) => AlertDialog(
            title: Text(title),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text(defaultActionText))
            ],
          ));
}
