import 'package:coviapp_th/services/api.dart';
import 'package:coviapp_th/services/api_service.dart';
import 'package:coviapp_th/services/data_model.dart';

class DataRepository {
  // DataRepository({@required this.dataResult});
  Data dataResult;
  final apiService = APIService(API.sandbox());

  //Map<Endpoint, dynamic> convertList = null;
  Future<Map<Endpoint, dynamic>> getEndpointsData({Data dataResult}) async {
    return {
      Endpoint.newConfirmed: dataResult.newConfirmed,
      Endpoint.newDeaths: dataResult.newDeaths,
      Endpoint.newRecovered: dataResult.newRecovered,
      Endpoint.newHospitalized: dataResult.newHospitalized,
      Endpoint.confirmed: dataResult.confirmed,
      Endpoint.recovered: dataResult.recovered,
      Endpoint.deaths: dataResult.deaths,
      Endpoint.hospitalized: dataResult.hospitalized,
      Endpoint.updateDate: dataResult.updateDate,
    };
  }

  //final apiService = APIService(API.sandbox());
  //DataRepository _dataRepository;
  Future<Map<Endpoint, dynamic>> _dataRepository;
  Future<Map<Endpoint, dynamic>> get getUpdatedData async {
    print('mcoke0000');
    _dataRepository = apiService
        .getEndpointData(endpoint: Endpoint.today)
        .then((value) => getEndpointsData(dataResult: value));
    //_dataRepository = dataResult;
    //    new DataRepository(dataResult: dataResult).getEndpointsData;
    return _dataRepository;
  }
}
